# SYMFONY PROJECT (PHP framework): CDA 17, HumanBooster

see [this page](https://formation-hb.drakolab.fr/symfony/98-projet.html#mon-compte) for more informations 

## Page annonce
Une page d'annonce doit comporter les éléments suivants :

Le titre de l'annonce
des informations sur les différents chiens adoptables. Pour chacun d'entre eux, peuvent être présents :
son nom
sa ou ses races (au moins une, et prévoir "inconnue" pour les chiens croisés)
ses antécédents (texte descriptif de son passé)
s'il est LOF ou non (chien de race homologué)
une description complète (texte descriptif du chien et de son comportement)
s'il accepte les autres animaux
une à 5 images
Un lien permettant de répondre à l'annonce (voir partie "Répondre à l'annonce")
⚠️ un chien de l'annonce peut être adopté, mais pas tous. Seuls les chiens non adoptés peuvent faire l'objet d'une demande d'adoption.

#Répondre à l'annonce
Pour accéder à cette page, l'adoptant doit avoir un compte et doit être connecté. Il doit pouvoir :

remplir un formulaire lui permettant de :
choisir le ou les chiens de l'annonce qu'il souhaite adopter (ou obtenir des informations)
entrer ses coordonnées de contact (email, téléphone, ville et département de résidence, nom, prénom, etc.)
Un message (minimum 200 caractères, maximum 12 000) à destination de l'association/SPA lié à l'annonce
⚠️ un chien de l'annonce peut être adopté, mais pas tous. Seuls les chiens non adoptés peuvent faire l'objet d'une demande d'adoption.

#Inscription
Pour accéder à cette page, l'adoptant ne doit pas être connecté et ne doit pas avoir de compte.

Le formulaire d'inscription demande uniquement un email et un mot de passe. Dès la demande effectuée, l'adoptant doit être connecté et pouvoir se servir de son compte.

#Mon compte
En tant qu'adoptant (ayant un compte et étant connecté), il dispose d'une section où il peut suivre les demande d'adoption déposées. Pour chacune d'entre elles, il doit pouvoir :

avoir une fiche récapitulative du ou des chiens concernés
accéder à un fil de discussion, contenant son message et les éventuelles réponses de l'association/SPA (⚠️ un adoptant ne peut plus envoyer de message après son premier contact, tant que l'association/SPA ne lui a pas répondu au moins une fois)
modifier ses informations personnelles (qui permettront de remplir plus rapidement le formulaire d'adoption)
