<?php


namespace App\Tests;


use App\Repository\CatRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CatRepositoryTest extends KernelTestCase
{

    public function testCount(){
        self::bootKernel(); // j recup le noyau
        $cats = self::$container->get(CatRepository::class)->count([]);// j'ai mon repo déjà configuré et j'appel method count avec aucun critere, sensé m donné 1 nombre de chat en bbd
        $this->assertEquals(18, $cats);
    }
}
