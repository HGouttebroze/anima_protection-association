<?php

namespace App\Repository;

use App\Entity\Adoption;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Adoption|null find($id, $lockMode = null, $lockVersion = null)
 * @method Adoption|null findOneBy(array $criteria, array $orderBy = null)
 * @method Adoption[]    findAll()
 * @method Adoption[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdoptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Adoption::class);
    }

    public function findNotTaken(int $number = 0, string $orderBy = null): array
    {
        $qb = $this->createQueryBuilder('a')
            ->innerJoin('a.dogs', 'd')
            ->andWhere('d.taken = :taken')
            ->setParameter('taken', false);
        if ($number > 0) {
            $qb->setMaxResults($number);
        }
        $qb->orderBy('a.name', 'ASC');
        if (!empty($orderBy)) {
            $qb->orderBy('a.' . $orderBy, 'DESC');
        }

        return $qb->getQuery()->getResult();
    }
        /*        if (true === $orderByUpdatedAt) {
                    $qb->orderBy('a.updatedAt', 'DESC');
                }

                return $qb->getQuery()->getResult();
            }*/


/*        public function findByLast($value)
        {
            $qb = $this->createQueryBuilder('a')
                ->andWhere('a.exampleField = :val')
                ->setParameter('val', $value)
                ->orderBy('a.id', 'ASC')
                ->setMaxResults(10)
                ->getQuery()
                ->getResult()
            ;
        }*/


        /*
        public function findOneBySomeField($value): ?Adoption
        {
            return $this->createQueryBuilder('a')
                ->andWhere('a.exampleField = :val')
                ->setParameter('val', $value)
                ->getQuery()
                ->getOneOrNullResult()
            ;
        }
        */

}

