<?php


namespace App\DataFixtures;

use App\Repository\AssociationRepository;
use App\Repository\CatRepository;
use App\Repository\DogRepository;
use DateTime;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Adoption;
use Doctrine\Bundle\FixturesBundle\Fixture;


class AdoptionFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var DogRepository
     */
    private $dogRepository;
    /**
     * @var AssociationRepository
     */
    private $associationRepository;
    /**
     * @var CatRepository
     */
    private $catRepository;

    public function __construct(DogRepository $dogRepository, AssociationRepository $associationRepository, CatRepository $catRepository)
    {
        $this->dogRepository = $dogRepository;
        $this->associationRepository = $associationRepository;
        $this->catRepository = $catRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $dogs = $this->dogRepository->findAll();
        $associations = $this->associationRepository->findAll();
        $cats = $this->catRepository->findAll();

        $date = new DateTime('2012-08-12');

        $adoption = new Adoption();
        $adoption->setName('Gros poilu trop gentil!');
        $adoption->setDescription('Gros poilu trop gentil cherche foyer avec carresses et papouille!');

        $adoption->addDog($dogs[0]);
        $adoption->setAssociation($associations[0]);

        $manager->persist($associations[0]);
        $manager->persist($dogs[0]);
        $manager->persist($adoption);


        $adoption1 = new Adoption();
        $adoption1->setName('Mais encore 1 gros poilu trop gentil!');
        $adoption1->setDescription('Mais encore 1 Gros poilu trop gentil cherche foyer avec carresses et papouille!');
        $adoption1->addDog($dogs[1]);
        $adoption->setAssociation($associations[1]);

        $manager->persist($associations[1]);
        $manager->persist($dogs[1]);
        $manager->persist($adoption1);

        for ($i = 1; $i < 15; $i++) {
            $adoption2 = new Adoption();
            $adoption2->setName('Annonce de gentils rescapé poilus - ' . $i);
            $adoption2->setDescription('poilus' . $i . 'trop gentil cherche foyer avec carresses et papouille!');

            $randomNumber = mt_rand(0, count($dogs) - 1);
            $adoption2->addDog($dogs[$randomNumber]);

            $randomNumber1 = mt_rand(0, count($associations) - 1);
            $adoption2->setAssociation($associations[$randomNumber1]);

            $manager->persist($adoption2);
        }

        // create 16 announcements with a cats
        for ($i = 1; $i < 16; $i++) {
            $catAdoption = new Adoption();

            $catAdoption->setName('Annonce de chats - ' . $i);
            $catAdoption->setDescription('Miaou ' . $i . ' cherche foyer avec carresses et papouille!');

            $randomNumber = mt_rand(0, count($cats) - 1);
            $catAdoption->addCat($cats[$randomNumber]);

            $manager->persist($catAdoption);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            DogFixtures::class,
            AssociationFixtures::class,
            CatFixtures::class
        ];
    }
}
