<?php

namespace App\Controller;

use App\Entity\Cat;
use App\Form\CatType;
use Doctrine\ORM\EntityManagerInterface;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/cat", name="cat")
 */
class CatController extends AbstractController
{
/*    public function index(): Response
    {
        return $this->render('cat/index.html.twig', [
            'controller_name' => 'CatController',
        ]);
    }*/

    /**
     * @Route("/new", name="new-cat", methods={"GET","POST"})
     * @Route("/{id}/edit", name="cat_edit")
     * @IsGranted("ROLE_ADMIN")
     * @param Cat|null $cat
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function form(Request $request, EntityManagerInterface $em, ?Cat $cat = null): Response
    {
        if (empty($cat)) {
            $cat = new Cat();
        }
        $form = $this->createForm(CatType::class, $cat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($cat);
            $em->flush();

            return $this->redirectToRoute('default_index');
        }

        return $this->render('cat/form.html.twig', [
            'cat' => $cat,
            'form' => $form->createView(),
        ]);

    }
}
