<?php

namespace App\Controller;

use App\Entity\Dog;
use App\Entity\User;
use App\Form\DogType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/chien", name="dog")

 */
class DogController extends AbstractController
{
    /**
     * @Route("/nouveau", name="dog_new")
     * @Route("/{id}/edit", name="dog_edit")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param Dog|null $dog
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function form(Request $request, EntityManagerInterface $em, ?Dog $dog = null): Response
    {
        if (empty($dog)) {
            $dog = new Dog();
        }

        $form = $this->createForm(DogType::class, $dog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($dog);
            $em->flush();

            return $this->redirectToRoute('default_index');
        }
        return $this->render('dog/form.html.twig', [
            'dog' => $dog,
            'form' => $form->createView(),
        ]);
    }
}
