<?php

namespace App\Controller\Admin;

use App\Entity\Adoption;
use App\Entity\Breed;
use App\Entity\Contact;
use App\Entity\Dog;
use App\Entity\Image;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;


class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin_dashboard")
     */
    public function index(): Response
    {
        /* PHP lines code copied from Symfony dashboard's default interface, to this index function  */

        // ...
        // redirect to some CRUD controller
        /*$routeBuilder = $this->get(AdminUrlGenerator::class);

        return $this->redirect($routeBuilder->setController(OneOfYourCrudController::class)->generateUrl());*/

        // you can also redirect to different pages depending on the current user
        /*if ('jane' === $this->getUser()->getUsername()) {
            return $this->redirect('...');
        }*/

        // you can also render some template to display a proper Dashboard
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        // return $this->render('some/path/my-dashboard.html.twig');

        /*
        TODO !!! WARNING !!! IMPORTANT !!! NOW: Return to Original code, now dead code, TO DELETE if no used !!!
        */
         /*return parent::index();*/
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Anima');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        /*yield MenuItem::linkToCrud('Message', 'fas fa-list', Contact::class);*/

        yield MenuItem::linkToCrud('Messages', 'fas fa-envelope', Contact::class);
        yield MenuItem::linkToCrud('Annonces', 'fas fa-dog', Adoption::class);
        yield MenuItem::linkToCrud('Chiens', 'fas fa-dog', Dog::class);
        yield MenuItem::linkToCrud( 'Origines','fas fa-paw', Breed::class);
        yield MenuItem::linkToCrud( 'Utilisateurs','fas fa-user', User::class);
/*        yield MenuItem::linkToCrud( 'Chats','fas fa-user', Cat::class);*/

        yield MenuItem::linkToCrud( 'Images','fas fa-user', Image::class);





    }

}
