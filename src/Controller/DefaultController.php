<?php

namespace App\Controller;


use App\Entity\Adoptant;
use App\Entity\Adoption;
use App\Entity\Contact;

use App\Entity\ContactPreAdoption;
use App\Form\ContactFormType;
use App\Form\ContactType;
use App\Repository\AdoptionRepository;
use App\Repository\AssociationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormTypeInterface;


class DefaultController extends AbstractController
{

    /**
     * @Route("/", name="default_index")
     * @param AdoptionRepository $adoptionRepository
     * @return Response
     */
    public function index(AdoptionRepository $adoptionRepository, AssociationRepository $associationRepository): Response
    {
       $adoptions = $adoptionRepository->findNotTaken(5, 'updatedAt');
       $associations = $associationRepository->findAll();



        return $this->render('default/index.html.twig', [
            'adoptions' => $adoptions,
            'associations' => $associations
        ]);
    }

    /**
     * @Route("/contact", name="default_contact")
     * @Route("/contact/{id}", name="default_contact_for_adoption")
     */
    public function contact(Request $request, EntityManagerInterface $entityManager, Adoption $adoption = null): Response
    {

        $this->denyAccessUnlessGranted('ROLE_USER'); // like if we put annotation @IsGranted but we can define roles redirection

        /*$contact = new Contact();

        $contact->setAdoption($adoption);

        $form = $this->createForm(ContactFormType::class, $contact);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $entityManager->persist($contact);
            $entityManager->flush();


            $this->addFlash('success', 'Votre message a bien été envoyé');

            return $this->redirectToRoute('default_contact');
        }

        return $this->render('default/contact.html.twig', [
            'form' => $form->createView(),
        ]);*/
        $contact = new Contact();

        $contact->setAdoption($adoption);

        $form = $this->createForm(ContactFormType::class, $contact);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $entityManager->persist($contact);
            $entityManager->flush();


            $this->addFlash('success', 'Votre message a bien été envoyé');

            return $this->redirectToRoute('default_contact');
        }

        return $this->render('default/contact.html.twig', [
            'form' => $form->createView(),
        ]);


        /*        $contact = new Contact();
                $contact->setAdoption($adoption);

                $form = $this->createForm(ContactType::class, $contact);

                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $entityManager->persist($contact);
                    $entityManager->flush();

                    $this->addFlash('success', 'msg envoyé');

                    return $this->redirectToRoute('default_contact');
                }

                return $this->render('default/contact.html.twig', [
                    'form' => $form->createView(),
                ]);*/
    }

    /**
     * @Route("/conditions", name="default_conditions")
     */
    public function conditions(): Response
    {
        return $this->render('default/conditions.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @Route("/preadoption", name="default_preadoption")
     */
    public function preadoption(Request $request, EntityManagerInterface $entityManager, Adoption $adoption = null): Response
    {
        $preadoption = new ContactPreAdoption();
        $preadoption->setAdoption($adoption);

        $form = $this->createForm(ContactPreAdoption::class, $preadoption);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($preadoption);
            $entityManager->flush();

            $this->addFlash('success', 'Votre demande d\'adoption a bien été envoyée, Nous vous contacterons au plus vite');

            return $this->redirectToRoute('default_preadoption');
        }

        return $this->render('default/preadoption.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}

