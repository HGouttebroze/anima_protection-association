<?php

namespace App\Form;

use App\Entity\Adoption;
use App\Entity\ContactPreAdoption;
use App\Form\Type\LocationType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PreAdoptionFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname')
            ->add('lastname')
            ->add('phone')
            ->add('email')
            ->add('location', LocationType::class, [
                'data_class' => ContactPreAdoption::class,
            ])
            ->add('bithDate')
            ->add('adoption',EntityType::class, [
        'class' => Adoption::class,
    ])
            ->add('description')
            ->add('association')
            ->add('createdAt')
            ->add('updatedAt')


        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PreAdoptionFormType::class,
        ]);
    }
}
