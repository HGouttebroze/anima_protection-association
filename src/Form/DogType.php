<?php

namespace App\Form;

use App\Entity\Association;
use App\Entity\Breed;
use App\Entity\Dog;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DogType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('history')
            ->add('LOF')
            ->add('sociabilityToAnimals')
            ->add('breeds', EntityType::class, [
                'class' => Breed::class,
                'multiple' => true,
                'expanded' => false,
            ])
/*            ->add('association', EntityType::class, [
                'class' => Association::class,
                'multiple' => true,
                'expanded' => false,
                'required' => false,
            ])*/
            ->add('birthDate', DateType::class, [
                'required' => false,
                'html5' => true,
                'widget' => 'single_text',
            ])

            ->add('images', CollectionType::class, [
                'entry_type' => ImageType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'required' => false,
                'by_reference' => false,
            ])
            ->add('adoption', null, [
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Dog::class,
        ]);
    }
}
