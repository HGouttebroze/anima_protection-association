<?php

namespace App\Security;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Guard\PasswordAuthenticatedInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class AppAuth extends AbstractFormLoginAuthenticator implements PasswordAuthenticatedInterface
{
    use TargetPathTrait;

    // La route de login par défaut. A adapter à vos besoins (ici, je remplace par security_login, personnellement)
    public const LOGIN_ROUTE = 'app_login';

    // Le manager est nécessaire pour récupérer notre utilisateur. On peut le remplacer par le UserRepository
    private $entityManager;

    // Service de génération d'URL / de chemins
    private $urlGenerator;

    // Service de génération de jeton CSRF, pour sécuriser les formulaires
    private $csrfTokenManager;

    // Service d'encodage des mots de passe
    private $passwordEncoder;

    public function __construct(EntityManagerInterface $entityManager, UrlGeneratorInterface $urlGenerator, CsrfTokenManagerInterface $csrfTokenManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->entityManager = $entityManager;
        $this->urlGenerator = $urlGenerator;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    // Cette méthode vérifie si notre authenticator est bien celui qui doit être appelé quand on appelle une route
    public function supports(Request $request): bool
    {
        // Il regarde si la route appelée est bien celle de connexion et est bien appelée au format POST
        // (soumission du formulaire)
        return self::LOGIN_ROUTE === $request->attributes->get('_route')
            && $request->isMethod('POST');
    }

    // `getCredentials` function check if password is valid
    public function getCredentials(Request $request)
    {
        $credentials = [
            // On récupère les données envoyées via POST
            // Si vous modifiez les noms des champs de votre formulaire,
            // c'est ici qu'il faudra faire les modifications nécessaires
            'email' => $request->request->get('email'),
            'password' => $request->request->get('password'),
            'csrf_token' => $request->request->get('_csrf_token'),
        ];
        // On stocke en session le dernier identifiant utilisé
        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['email']
        );

        return $credentials;
    }

    // On va essayer de récupérer l'utilisateur qu'on essaie de connecter
    public function getUser($credentials, UserProviderInterface $userProvider): ?User
    {
        // On vérifie si le jeton CSRF est bien le bon
        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }

        // On récupère l'utilisateur dont l'utilisateur a entré l'identifiant et le mot de passe
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $credentials['email']]);

        // Si on ne le trouve pas, on renvoie une exception
        // qui sera attrapée et convertie en un message d'erreur
        if (!$user) {
            // fail authentication with a custom error
            throw new CustomUserMessageAuthenticationException('Email could not be found.');
        }

        return $user;
    }

    // Vérifie si le mot de passe entré est le bon
    public function checkCredentials($credentials, UserInterface $user): bool
    {
        return $this->passwordEncoder->isPasswordValid($user, $credentials['password']);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function getPassword($credentials): ?string
    {
        return $credentials['password'];
    }

    // Ici, on ajoute un chemin vers lequel envoyer l'utilisateur après connexion
    // Il faut bien penser à l'ajouter
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey): ?Response
    {
        if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
            return new RedirectResponse($targetPath);
        }

        // throw new \Exception('TODO: provide a valid redirect inside '.__FILE__);
        return new RedirectResponse($this->urlGenerator->generate('default_index'));
    }

    // On récupère l'url (le chemin) de la route de login
    protected function getLoginUrl(): string
    {
        return $this->urlGenerator->generate(self::LOGIN_ROUTE);
    }
}



