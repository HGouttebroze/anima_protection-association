<?php

namespace App\Entity;

use App\Entity\Traits\HasIdTrait;
use App\Entity\Traits\HasNameTrait;
use App\Repository\DogRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=DogRepository::class)
 */
class Dog
{
    use HasNameTrait;
    use HasIdTrait;


    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthDate;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Adoption::class,
     *     inversedBy="dogs", cascade={"persist"})
     */
    private $adoption;

    /**
     * @ORM\ManyToMany(targetEntity=Breed::class, mappedBy="dogs")
     */
    private $breeds;

    /**
     * @ORM\OneToMany(targetEntity=Image::class, mappedBy="dog", cascade={"persist"})
     */
    private $images;

    /**
     * @ORM\Column(type="boolean")
     */
    private $taken;
    private $user;

    /**
     * @ORM\Column(type="text")
     */
    private $history;

    /**
     * @ORM\Column(type="boolean")
     */
    private $LOF;

    /**
     * @ORM\Column(type="boolean")
     */
    private $sociabilityToAnimals;

    /**
     * @ORM\ManyToOne(targetEntity=Association::class, inversedBy="Dog")
     */
    private $association;




    public function __construct()
    {
        $this->breeds = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->taken = false;
    }


    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(?\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAdoption(): ?Adoption
    {
        return $this->adoption;
    }

    public function setAdoption(?Adoption $adoption): self
    {
        $this->adoption = $adoption;

        return $this;
    }

    /**
     * @return Collection|Breed[]
     */
    public function getBreeds(): Collection
    {
        return $this->breeds;
    }

    public function addBreed(Breed $breed): self
    {
        if (!$this->breeds->contains($breed)) {
            $this->breeds[] = $breed;
            $breed->addDog($this);
        }

        return $this;
    }

    public function removeBreed(Breed $breed): self
    {
        if ($this->breeds->removeElement($breed)) {
            $breed->removeDog($this);
        }

        return $this;
    }


    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setDog($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getDog() === $this) {
                $image->setDog(null);
            }
        }

        return $this;
    }


/* add dog's age */
    public function getAge(): ?int
    {
        if (empty($this->getBirthDate())) {
            return null;
        }

        $now = new DateTime();

        $diff = $this->getBirthDate()->diff($now, true);

        return $diff->y;
    }

    public function getTaken(): ?bool
    {
        return $this->taken;
    }

    public function setTaken(bool $taken): self
    {
        $this->taken = $taken;

        return $this;
    }
    public function __toString()
    {
        return $this->getName();
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getHistory(): ?string
    {
        return $this->history;
    }

    public function setHistory(string $history): self
    {
        $this->history = $history;

        return $this;
    }

    public function getLOF(): ?bool
    {
        return $this->LOF;
    }

    public function setLOF(bool $LOF): self
    {
        $this->LOF = $LOF;

        return $this;
    }

    public function getSociabilityToAnimals(): ?bool
    {
        return $this->sociabilityToAnimals;
    }

    public function setSociabilityToAnimals(bool $sociabilityToAnimals): self
    {
        $this->sociabilityToAnimals = $sociabilityToAnimals;

        return $this;
    }

    public function getAssociation(): ?Association
    {
        return $this->association;
    }

    public function setAssociation(?Association $association): self
    {
        $this->association = $association;

        return $this;
    }



}
