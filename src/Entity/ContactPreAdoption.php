<?php


namespace App\Entity;


use App\Entity\Traits\HasIdTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

class ContactPreAdoption
{

    use HasIdTrait;
    use TimestampableEntity;

    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Length(min=2, max=100)
     */
    private $firstname;

    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Length(min=2, max=100)
     */
    private $lastname;

    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Regex(
     *  pattern="/[0-9]{10}/"
     * )
     */
    private $phone;

    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;


    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     * @Assert\Length(min=50, max=2500)
     */
    private $message;

    /**
     * @var string|null
     */
    private $occupation;

    /**
     * @var
     * @Assert\NotBlank()
     */
    private $address;

    /**
     * @var
     * @Assert\NotBlank()
     */
    private $addressSupplement;
    private $zipcode;
    private $city;
    private $country;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthDate;


    /**
     * @ORM\ManyToOne(targetEntity=Adoption::class, inversedBy="contactPreAdoptions")
     */
    private $adoption;

    /**
     * @return string|null
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param string|null $firstname
     * @return ContactPreAdoption
     */
    public function setFirstname(?string $firstname): ContactPreAdoption
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @param string|null $lastname
     * @return ContactPreAdoption
     */
    public function setLastname(?string $lastname): ContactPreAdoption
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     * @return ContactPreAdoption
     */
    public function setPhone(?string $phone): ContactPreAdoption
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return ContactPreAdoption
     */
    public function setEmail(?string $email): ContactPreAdoption
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     * @return ContactPreAdoption
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOccupation(): ?string
    {
        return $this->occupation;
    }

    /**
     * @param string|null $occupation
     * @return ContactPreAdoption
     */
    public function setOccupation(?string $occupation): ContactPreAdoption
    {
        $this->occupation = $occupation;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     * @return ContactPreAdoption
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddressSupplement()
    {
        return $this->addressSupplement;
    }

    /**
     * @param mixed $addressSupplement
     * @return ContactPreAdoption
     */
    public function setAddressSupplement($addressSupplement)
    {
        $this->addressSupplement = $addressSupplement;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param mixed $zipcode
     * @return ContactPreAdoption
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return ContactPreAdoption
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     * @return ContactPreAdoption
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param mixed $birthDate
     * @return ContactPreAdoption
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdoption()
    {
        return $this->adoption;
    }

    /**
     * @param mixed $adoption
     * @return ContactPreAdoption
     */
    public function setAdoption($adoption)
    {
        $this->adoption = $adoption;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return ContactPreAdoption
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return ContactPreAdoption
     */
    public function setCreatedAt(\DateTime $createdAt): ContactPreAdoption
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return ContactPreAdoption
     */
    public function setUpdatedAt(\DateTime $updatedAt): ContactPreAdoption
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }






}
