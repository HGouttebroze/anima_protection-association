<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210911183320 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE association (id INT AUTO_INCREMENT NOT NULL, announcements_provided INT DEFAULT NULL, email LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE adoption ADD association_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE adoption ADD CONSTRAINT FK_EDDEB6A9EFB9C8A5 FOREIGN KEY (association_id) REFERENCES association (id)');
        $this->addSql('CREATE INDEX IDX_EDDEB6A9EFB9C8A5 ON adoption (association_id)');
        $this->addSql('ALTER TABLE dog ADD association_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE dog ADD CONSTRAINT FK_812C397DEFB9C8A5 FOREIGN KEY (association_id) REFERENCES association (id)');
        $this->addSql('CREATE INDEX IDX_812C397DEFB9C8A5 ON dog (association_id)');
        $this->addSql('ALTER TABLE user ADD association_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649EFB9C8A5 FOREIGN KEY (association_id) REFERENCES association (id)');
        $this->addSql('CREATE INDEX IDX_8D93D649EFB9C8A5 ON user (association_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adoption DROP FOREIGN KEY FK_EDDEB6A9EFB9C8A5');
        $this->addSql('ALTER TABLE dog DROP FOREIGN KEY FK_812C397DEFB9C8A5');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649EFB9C8A5');
        $this->addSql('DROP TABLE association');
        $this->addSql('DROP INDEX IDX_EDDEB6A9EFB9C8A5 ON adoption');
        $this->addSql('ALTER TABLE adoption DROP association_id');
        $this->addSql('DROP INDEX IDX_812C397DEFB9C8A5 ON dog');
        $this->addSql('ALTER TABLE dog DROP association_id');
        $this->addSql('DROP INDEX IDX_8D93D649EFB9C8A5 ON user');
        $this->addSql('ALTER TABLE user DROP association_id');
    }
}
