<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210914095121 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cat ADD adoption_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cat ADD CONSTRAINT FK_9E5E43A8631C55DF FOREIGN KEY (adoption_id) REFERENCES adoption (id)');
        $this->addSql('CREATE INDEX IDX_9E5E43A8631C55DF ON cat (adoption_id)');
        $this->addSql('ALTER TABLE picture ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME NOT NULL, DROP filename');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cat DROP FOREIGN KEY FK_9E5E43A8631C55DF');
        $this->addSql('DROP INDEX IDX_9E5E43A8631C55DF ON cat');
        $this->addSql('ALTER TABLE cat DROP adoption_id');
        $this->addSql('ALTER TABLE picture ADD filename VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, DROP created_at, DROP updated_at');
    }
}
