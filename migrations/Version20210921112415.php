<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210921112415 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE adoptant (id INT AUTO_INCREMENT NOT NULL, adoption_id INT DEFAULT NULL, firstname VARCHAR(128) NOT NULL, lastname VARCHAR(128) NOT NULL, phone VARCHAR(128) NOT NULL, email VARCHAR(128) NOT NULL, address VARCHAR(255) NOT NULL, address_supplement VARCHAR(255) DEFAULT NULL, zipcode VARCHAR(128) NOT NULL, city VARCHAR(128) NOT NULL, country VARCHAR(128) NOT NULL, bith_date DATETIME NOT NULL, INDEX IDX_7B42F2A631C55DF (adoption_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE adoptant ADD CONSTRAINT FK_7B42F2A631C55DF FOREIGN KEY (adoption_id) REFERENCES adoption (id)');
        $this->addSql('ALTER TABLE cat DROP filename, DROP updated_at, DROP created_at');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE adoptant');
        $this->addSql('ALTER TABLE cat ADD filename VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, ADD updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', ADD created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
    }
}
